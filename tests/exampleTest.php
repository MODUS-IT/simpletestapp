<?php

include_once './exampleClass.php';

use PHPUnit\Framework\TestCase;

class exampleTest extends TestCase
{

    function testGetTitle()
    {
        $example = new exampleClass('HelloWorld');
        $this->assertEquals('HelloWorld',$example->getTitle());
    }

}